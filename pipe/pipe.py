import base64
import os
import sys
import subprocess

import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


MAX_LABEL_LENGTH = 63

logger = get_logger()

schema = {
    'KUBE_CONFIG': {'type': 'string', 'required': True},
    'KUBECTL_COMMAND': {'type': 'string', 'required': True},
    'KUBECTL_ARGS': {'type': 'list', 'required': False, 'default': []},
    'KUBECTL_APPLY_ARGS': {'type': 'string', 'required': False, 'default': '-f'},
    'RESOURCE_PATH': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
    'LABELS': {'type': 'list', 'required': False},
    'WITH_DEFAULT_LABELS': {'type': 'boolean', 'required': False, 'default': True},
    'DISABLE_VALIDATION': {'type': 'boolean', 'required': False, 'default': False},
    'PRE_EXECUTION_SCRIPT': {'type': 'string', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class KubernetesDeployPipe(Pipe):

    def update_labels_in_metadata(self, template, labels):

        with open(template, 'r') as template_file:
            yamls = list(yaml.safe_load_all(template_file.read()))
            if None in yamls:
                yamls.remove(None)
                self.log_warning(f"Template {template} contains empty or incorrect directives in the YAML file. They will be skipped from the template.")

            for yaml_doc in yamls:
                if 'metadata' in yaml_doc:
                    yaml_doc['metadata'].setdefault('labels', {}).update(labels)

        with open(template, 'w') as template_file:
            yaml.dump_all(yamls, template_file)

    def compile_labels(self):
        labels = dict(tuple(label.split('=')) for label in self.get_variable('LABELS'))

        if self.get_variable('WITH_DEFAULT_LABELS'):
            bitbucket_branch = os.getenv('BITBUCKET_BRANCH')

            if bitbucket_branch is not None:
                if len(bitbucket_branch) > MAX_LABEL_LENGTH:
                    self.log_warning(f'To execute kubectl command check that bitbucket branch name is less than 63 characters.'
                                     f' Current length: {len(bitbucket_branch)}. Label `bitbucket_branch` will be truncated to {bitbucket_branch[:MAX_LABEL_LENGTH]}')
                    bitbucket_branch = bitbucket_branch[:MAX_LABEL_LENGTH]

                labels['bitbucket.org/bitbucket_branch'] = bitbucket_branch

                if '/' in bitbucket_branch:
                    self.log_warning('"/" is not allowed in kubernetes labels. Slashes will be replaced by a dash "-" in the "bitbucket.org/bitbucket_commit" label value.'
                                     'For more information you can check the official kubernetes docs'
                                     'https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set')
                    labels['bitbucket.org/bitbucket_branch'] = labels['bitbucket.org/bitbucket_branch'].replace('/', '-')

                if labels['bitbucket.org/bitbucket_branch'].endswith('-'):
                    self.log_warning(
                        'Trailing "-" is not allowed in kubernetes labels value and will be stripped.'
                        'For more information you can check the official kubernetes docs'
                        'https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set')
                    labels['bitbucket.org/bitbucket_branch'] = labels['bitbucket.org/bitbucket_branch'].rstrip('-')

            step_triggerer_uuid = os.getenv('BITBUCKET_STEP_TRIGGERER_UUID')
            if step_triggerer_uuid is not None:
                # trim the starting and closing curly braces
                labels['bitbucket.org/bitbucket_step_triggerer_uuid'] = step_triggerer_uuid[1:-1]

            variables = [
                ('BITBUCKET_COMMIT', 'bitbucket.org/bitbucket_commit'),
                ('BITBUCKET_REPO_OWNER', 'bitbucket.org/bitbucket_repo_owner'),
                ('BITBUCKET_REPO_SLUG', 'bitbucket.org/bitbucket_repo_slug'),
                ('BITBUCKET_BUILD_NUMBER', 'bitbucket.org/bitbucket_build_number'),
                ('BITBUCKET_DEPLOYMENT_ENVIRONMENT', 'bitbucket.org/bitbucket_deployment_environment')
            ]

            for variable, label_name in variables:
                value = os.getenv(variable)
                if value is not None:
                    labels[label_name] = value

        return labels

    def validate_spec_file(self, resource_path):
        # validate config "kubectl apply -f nginx.yml --dry-run=server"
        debug = ['--v=2'] if self.get_variable('DEBUG') else []

        result = subprocess.run(['kubectl', 'apply', self.get_variable('KUBECTL_APPLY_ARGS'), resource_path] + list(
            self.get_variable('KUBECTL_ARGS')) + debug + ['--dry-run=server'], stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('spec file validation failed.')
        else:
            self.log_info('spec file is valid.')

    def handle_apply(self):
        resource_path = self.get_variable('RESOURCE_PATH')
        if not resource_path:
            self.fail('Path to the spec file is required when running the "apply" command')

        is_path_dir = os.path.isdir(resource_path)

        if is_path_dir:
            templates = [template for template in os.listdir(resource_path)
                         if template.endswith(('.yml', '.yaml')) and not
                         template.endswith(('kustomization.yml', 'kustomization.yaml'))]
            templates_absolute_paths = [os.path.join(resource_path, template_file) for template_file in templates]
        else:
            templates_absolute_paths = [resource_path]

        labels = self.compile_labels()

        if labels:
            for template_file in templates_absolute_paths:
                self.update_labels_in_metadata(template_file, labels)

        if not self.get_variable('DISABLE_VALIDATION'):
            self.validate_spec_file(resource_path)

        validate = ['--validate=false'] if self.get_variable('DISABLE_VALIDATION') else []
        debug = ['--v=2'] if self.get_variable('DEBUG') else []

        result = subprocess.run(['kubectl', 'apply', self.get_variable('KUBECTL_APPLY_ARGS'), resource_path] + list(
            self.get_variable('KUBECTL_ARGS')) + validate + debug, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('kubectl apply failed.')
        else:
            self.success('kubectl apply was successful.')

    def handle_generic(self, cmd):
        debug = ['--v=2'] if self.get_variable('DEBUG') else []
        result = subprocess.run(['kubectl'] + cmd.split() + list(self.get_variable('KUBECTL_ARGS')) + debug,
                                stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f'kubectl {cmd} failed.')
        else:
            self.success(f'kubectl {cmd} was successful.')

    def configure(self):
        self.log_info("Configuring kubeconfig...")
        kube_config_path = '/tmp/kube_config'

        with open(kube_config_path, 'w+') as kube_config:
            kube_config.write(base64.b64decode(self.get_variable('KUBE_CONFIG')).decode())

        os.environ['KUBECONFIG'] = kube_config_path

    def run(self):

        self.configure()

        pre_execution_script = self.get_variable('PRE_EXECUTION_SCRIPT')
        if pre_execution_script:
            if not os.path.exists(pre_execution_script):
                self.fail(f'Passed pre-execution script file {pre_execution_script} does not exist.')

            try:
                logger.info(f'Executing pre-execution script before pipe running sh {pre_execution_script}')
                result = subprocess.run(['sh', pre_execution_script], check=True, capture_output=True, text=True)

                if self.get_variable('DEBUG'):
                    logger.debug(f'Executed pre-execution script. Output: {result.stdout}')

            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

        cmd = self.get_variable('KUBECTL_COMMAND')

        if os.getenv('KUBECTL_ARGS') and not len(self.get_variable('KUBECTL_ARGS')):
            self.fail('Variable KUBECTL_ARGS must be an array.')

        if cmd == 'apply':
            self.handle_apply()
        else:
            self.handle_generic(cmd)

        self.success(message="Pipe finished successfully!")


if __name__ == '__main__':
    pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)
    pipe.run()
