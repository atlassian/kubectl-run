# Bitbucket Pipelines Pipe: Kubernetes Deploy

Run a command against a Kubernetes cluster. This pipe uses [kubectl][kubectl], a command line interface for running commands against Kubernetes clusters.
Now pipe uses [AWS CLI version 2][AWS CLI version 2], so if you are deploying to AWS resources, refer to [breaking changes of AWS CLI v2][AWS CLI breaking changes].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/kubectl-run:3.12.0
  variables:
    KUBE_CONFIG: '<string>'
    KUBECTL_COMMAND: '<string>'
    # RESOURCE_PATH: '<string>' # Optional.
    # KUBECTL_APPLY_ARGS: '<string>' # Optional.
    # KUBECTL_ARGS: '<array>' # Optional.
    # LABELS: '<array>' # Optional.
    # WITH_DEFAULT_LABELS: '<boolean>' # Optional.
    # DISABLE_VALIDATION: '<boolean>' # Optional.
    # PRE_EXECUTION_SCRIPT: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```
## Variables

| Variable             | Usage                                                                                                                                                                                                                                                                                                                                                                        |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| KUBE_CONFIG (*)      | Base 64 string containing the Kubernetes configuration. See **Prerequisites**.                                                                                                                                                                                                                                                                                               |
| KUBECTL_COMMAND (*)  | Kubectl command to run. For more details you can check the [kubectl reference guide][kubectl reference guide].                                                                                                                                                                                                                                                               |
| RESOURCE_PATH        | Path to the kubernetes spec file or a directory containing multiple spec files with your resource definitions. This option is required only if the KUBECTL_COMMAND is `apply`. Only `yaml` files are supported (files with other extensions will be ignored).                                                                                                                |
| KUBECTL_ARGS         | Arguments to pass to the kubectl command. YAML array (list). Default: `null`. See **Examples**.                                                                                                                                                                                                                                                                              |
| KUBECTL_APPLY_ARGS   | Arguments to pass after the kubectl `apply` command. Default: `-f`. For more details check out the [kubectl apply guide][kubectl apply guide]                                                                                                                                                                                                                                |
| LABELS               | Key=value pairs that are attached to a Deployment. Labels are intended to be used to specify identifying attributes of objects. Labels will be updated only if the KUBECTL_COMMAND is `apply`. Valid labels must follow [Syntax and character set][Syntax and character set]. Format: `key=value`. Example: `bitbucket.org/bitbucket_branch=develop`.                        |
| WITH_DEFAULT_LABELS  | Whether or not to add the default labels. Check Labels added by default section for more details.                                                                                                                                                                                                                                                                            |
| DISABLE_VALIDATION   | Set this variable to `true` to disable validation of your Kubernetes manifest file before the command execution. Default `false`.                                                                                                                                                                                                                                            |
| PRE_EXECUTION_SCRIPT | Path to pre-execution script to execute additional specific actions needed.                                                                                                                                                                                                                                                                                                  |
| DEBUG                | Turn on extra debug information. Default: `false`. If set to `true`, the pipe will add `--v=2` option to the kubectl command. If you want higher level of verbosity, you will need to pass the desired verbosity level via the `KUBECTL_ARGS` parameter. More info about the kubectl debug levels can be found in [the guide][kubectl-output-verbosity-and-debugging guide]. |

_(*) = required variable._

## Labels added by default

By default, the pipe uses the following labels in order to track which pipeline created the Kubernetes resources and be able to track it from Kubernetes.

| Label                                            | Description                                                                                                |
|--------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| `bitbucket.org/bitbucket_commit`                 | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f` |
| `bitbucket.org/bitbucket_deployment_environment` | The name of the environment which the step deploys to. This is only available on deployment steps.         | 
| `bitbucket.org/bitbucket_repo_owner`             | The name of the owner account.                                                                             |
| `bitbucket.org/bitbucket_repo_slug`              | Repository name.                                                                                           |
| `bitbucket.org/bitbucket_build_number`           | Bitbucket Pipeline number                                                                                  |
| `bitbucket.org/bitbucket_step_triggerer_uuid`    | UUID from the user who triggered the step execution.                                                       |
| `bitbucket.org/bitbucket_branch`                 | Repository branch name. Will be automatically truncated when length exceeds 63 characters.                 |

## Prerequisites

- Basic knowledge is required of how Kubernetes works and how to create services and deployments on it.
- A Kubernetes cluster up and running. In order to configure the credentials in Pipelines, you will need to add the base64-encoded `kubeconfig` file as a Repository Variable. This is how you can obtain it: on Linux `base64 -w 0 < ~/.kube/config` and
on Mac OS `base64 < ~/.kube/config`. Just copy the base64-encoded string from stdout and put in into the KUBE_CONFIG variable in your repository settings.
- A docker registry (Docker Hub or similar) to store your docker image: if you are deploying to a Kubernetes cluster you will need a docker registry to store you images.


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
```

Example with disabling validation of your spec file:
```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      DISABLE_VALIDATION: 'true'
```

Example passing a directory with your resources definitions:

```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'my-directory/'
```

Example passing preexecution hook script file (file should have at least read and execute permissions):

```yaml
script:
  - echo 'script logic' > .my-script.sh
  - chmod 005 .my-script.sh
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      PRE_EXECUTION_SCRIPT: '.my-script.sh'
```

Advanced example:

Passing KUBECTL_ARGS:

```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      KUBECTL_ARGS:
        - '--dry-run'
```

Using higher verbosity level:

```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      KUBECTL_ARGS:
        - '--v=9'
```

Providing custom labels:

```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      LABELS:
        - 'environment=production'
        - 'tier=backend'
```

Using kustomization arguments:
```yaml
script:
  - pipe: atlassian/kubectl-run:3.12.0
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'k8s/dev/kustomization'
      KUBECTL_APPLY_ARGS: '-k'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,kubernetes
[kubectl]: https://kubernetes.io/docs/reference/kubectl/overview/
[kubectl reference guide]: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
[kubectl apply guide]: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply
[Syntax and character set]: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
[kubectl-output-verbosity-and-debugging guide]: https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-output-verbosity-and-debugging
[AWS CLI version 2]: https://docs.aws.amazon.com/cli/latest/userguide/welcome-versions.html
[AWS CLI breaking changes]: https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration.html
