from distutils.core import setup

setup(
    name='kubectl_run',
    version='3.12.0',
    packages=['kubectl_run', ],
    package_dir={'kubectl_run': 'pipe'},
    url='https://bitbucket.org/atlassian/kubectl-run',
    author='Atlassian',
    author_email='bitbucketci-team@atlassian.com',
    description='Pipe kubectl-run',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    install_requires=[
        'bitbucket-pipes-toolkit>=4.3.0'
    ]
)
