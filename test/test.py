import os

from bitbucket_pipes_toolkit.test import PipeTestCase

from bitbucket_pipes_toolkit import ArrayVariable

template = """
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-ng-k8
  labels:
    app: nginx-k8
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-k8
  template:
    metadata:
      labels:
        app: nginx-k8
    spec:
      containers:
      - name: nginx-k8
        image: nginx:1.7.9
        ports:
        - containerPort: 80

"""


class KubeTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        with open('test/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(template)

    def tearDown(self):
        super().tearDown()
        os.remove('test/nginx.yml')

    def test_apply(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('kubectl apply was successful', result)

    def test_apply_fail_if_kubectl_args_is_not_a_list(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),
            "KUBECTL_ARGS": "string value",

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('Variable KUBECTL_ARGS must be an array', result)

    def test_apply_with_default_labels(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),
            "WITH_DEFAULT_LABELS": True,

            "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('kubectl apply was successful', result)

        environment = {
            "KUBECTL_COMMAND": "get deployments",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        }
        environment.update(ArrayVariable.from_list('KUBECTL_ARGS', ['--show-labels']).decompile())
        result = self.run_container(environment=environment)

        self.assertIn('bitbucket.org/bitbucket_repo_owner=atlassian', result)

    def test_apply_with_user_provided_labels(self):
        environment = {
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        }
        environment.update(ArrayVariable.from_list('LABELS', ['environment=production', 'tier=backend']).decompile())
        result = self.run_container(environment=environment)

        self.assertIn('kubectl apply was successful', result)

        environment = {
            "KUBECTL_COMMAND": "get deployments",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        }
        environment.update(ArrayVariable.from_list('KUBECTL_ARGS', ['--show-labels']).decompile())
        result = self.run_container(environment=environment)

        self.assertIn('environment=production', result)
        self.assertIn('tier=backend', result)

    def test_apply_with_slashes_in_branch_label(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "BITBUCKET_BRANCH": 'test/test',
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('WARNING: "/" is not allowed', result)

    def test_get_pods_successful(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('✔ Pipe finished successfully!', result)

    def test_deploy_pre_execution_script_does_not_exist(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            'PRE_EXECUTION_SCRIPT': '.does-not-exist.sh'
        })

        self.assertIn('Passed pre-execution script file .does-not-exist.sh does not exist.', result)

    def test_deploy_pre_execution_script_failed(self):
        pre_execution_file = '.pre-execution-script.sh'
        with open(pre_execution_file, 'w') as f:
            f.write('pip install kubernetes-not-exist')
        os.chmod(pre_execution_file, 0o0005)

        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': pre_execution_file
        })

        self.assertIn("ERROR: No matching distribution found for kubernetes-not-exist",
                      result)

    def test_deploy_pre_execution_script_success(self):
        pre_execution_file = '.pre-execution-script.sh'
        with open(pre_execution_file, 'w') as f:
            f.write("pip install kubernetes")
        os.chmod(pre_execution_file, 0o0005)

        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': pre_execution_file,
            'DEBUG': True
        })

        self.assertIn("Executed pre-execution script", result)


class KubeKustomizeTestCase(PipeTestCase):

    def test_apply_kustomization(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "KUBECTL_APPLY_ARGS": "-k",
            "RESOURCE_PATH": "test/kustomization",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('kubectl apply was successful', result)


wrong_labels_template = """
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-ng-k8
  labels:
    _app: nginx-k8
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-k8
  template:
    metadata:
      labels:
        app: nginx-k8
    spec:
      containers:
      - name: nginx-k8
        image: nginx:1.7.9
        ports:
        - containerPort: 80

"""


class KubeSpecsFileTestCase(PipeTestCase):

    maxDiff = None
    spec_file_path = 'test/wrong_nginx.yml'

    def setUp(self):
        super().setUp()
        with open(self.spec_file_path, 'w+') as nginx_yml:
            nginx_yml.write(wrong_labels_template)

    def tearDown(self):
        super().tearDown()
        os.remove(self.spec_file_path)

    def test_fail_non_valid_spec_file(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": self.spec_file_path,
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('Invalid value: "_app": name part must consist of alphanumeric characters', result)
        self.assertIn('spec file validation failed', result)
