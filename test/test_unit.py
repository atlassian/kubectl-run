import base64
import io
import logging
import os
import shutil
import sys
import yaml
from copy import copy
from contextlib import contextmanager
from unittest import TestCase

import pytest

from pipe.pipe import KubernetesDeployPipe, schema

BASE_TEMPLATE = """
apiVersion: v1
kind: Service
metadata:
  name: my-nginx-svc
  labels:
    app: nginx
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: nginx
"""

SIMPLE_TEMPLATE = """
apiVersion: v1
kind: Service
"""

WRONG_NOTATION_TEMPLATE = """
---
apiVersion: v1
kind: Service
metadata:
  name: my-nginx-svc
  labels:
    app: nginx
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: nginx
---


"""

INVALID_KUBECTL_TEMPLATE = """
apiVersion: v1
kind: Service
metadata:
  name: my-nginx-svc
  labels:
    _app: nginx

"""


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class KubectlBaseTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, fp):
        self.caplog = caplog
        self.mocker = mocker
        self.fp = fp

    def setUp(self):
        self.template = BASE_TEMPLATE
        self.kube_config = str(base64.b64encode('config string'.encode('utf-8')), 'utf-8')
        self.sys_path = copy(sys.path)

        sys.path.insert(0, os.getcwd())

        with open('test/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(self.template)

    def tearDown(self):
        os.remove('test/nginx.yml')
        sys.path = self.sys_path

    def test_apply_success_default(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_apply_success_f_flag(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "KUBECTL_APPLY_ARGS": "-f",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_apply_success_kubectl_args_array(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "KUBECTL_APPLY_ARGS": "-f",
                "KUBECTL_ARGS_0": "--aaa=111",
                "KUBECTL_ARGS_1": "--bbb=222",
                "KUBECTL_ARGS_COUNT": "2",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--aaa=111", "--bbb=222", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--aaa=111", "--bbb=222"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_apply_success_k_flag(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "KUBECTL_APPLY_ARGS": "-k",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-k", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-k", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_with_default_labels_equal_false(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "WITH_DEFAULT_LABELS": "false",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertFalse(pipe.get_variable('WITH_DEFAULT_LABELS'))
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_apply_with_slashes_in_branch_label(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "BITBUCKET_BRANCH": "test/test",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertTrue(pipe.get_variable('WITH_DEFAULT_LABELS'))
        self.assertIn('"/" is not allowed in kubernetes labels.', self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_apply_with_trailed_dash_in_label(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "BITBUCKET_BRANCH": "test_test-",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )
        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn('Trailing "-" is not allowed in kubernetes labels', self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_update_labels_in_metadata_add_label(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "LABELS_0": "environment=test",
                "LABELS_COUNT": "1",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        with open(pipe.get_variable('RESOURCE_PATH'), 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            expected_label = {"environment": "test"}

        self.assertTrue(updated_labels | expected_label == updated_labels)

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_update_labels_in_metadata_new_label_not_break_existing(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "LABELS_0": "environment=test",
                "LABELS_COUNT": "1",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        with open(pipe.get_variable('RESOURCE_PATH'), 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            existing_label = {"app": "nginx"}

        self.assertTrue(updated_labels | existing_label == updated_labels)

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_update_labels_step_triggerer_trims_curly_braces(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "LABELS_0": "environment=test",
                "LABELS_COUNT": "1",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        with open(pipe.get_variable('RESOURCE_PATH'), 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            actual_step_triggerer_uuid = updated_labels['bitbucket.org/bitbucket_step_triggerer_uuid']
            expected_step_triggerer_uuid = "asdasd-565656-asdad-565655"

        self.assertEqual(actual_step_triggerer_uuid, expected_step_triggerer_uuid)

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_update_labels_in_metadata_empty_labels(self):
        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "WITH_DEFAULT_LABELS": "false",
                "RESOURCE_PATH": "test/nginx.yml"
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml", "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", "test/nginx.yml"],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        yaml_data = yaml.safe_load(self.template)
        current_labels = yaml_data['metadata']['labels']

        with open(pipe.get_variable('RESOURCE_PATH'), 'r') as template_file_updated:
            yaml_data = yaml.safe_load(template_file_updated.read())
            updated_labels = yaml_data['metadata']['labels']

        self.assertEqual(current_labels, updated_labels)

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())


class KubectlAdditionalTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, fp):
        self.caplog = caplog
        self.mocker = mocker
        self.fp = fp

    def setUp(self):
        os.mkdir(os.path.join(f'{os.getcwd()}/test', "test-dir"))

        self.template_dir = f'{os.getcwd()}/test/test-dir/'
        self.template_path = f'{self.template_dir}nginx.yml'
        self.kube_config = str(base64.b64encode('config string'.encode('utf-8')), 'utf-8')
        self.sys_path = copy(sys.path)

        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        shutil.rmtree(os.path.join(f'{os.getcwd()}/test', "test-dir"))

    def test_not_update_labels_in_metadata_empty_metadata(self):
        with open(self.template_path, 'w+') as f:
            f.write(SIMPLE_TEMPLATE)

        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "WITH_DEFAULT_LABELS": "false",
                "LABELS_0": "environment=test",
                "LABELS_COUNT": "1",
                "RESOURCE_PATH": self.template_path
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_path, "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_path],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        yaml_data_current = yaml.safe_load(SIMPLE_TEMPLATE)

        with open(pipe.get_variable('RESOURCE_PATH'), 'r') as template_file:
            yaml_data_updated = yaml.safe_load(template_file.read())

        self.assertEqual(yaml_data_current, yaml_data_updated)

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_deploy_directory_success(self):
        with open(self.template_path, 'w+') as f:
            f.write(SIMPLE_TEMPLATE)

        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "RESOURCE_PATH": self.template_dir
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_dir, "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_dir],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_multiple_yamls_wrong_notation(self):
        with open(self.template_path, 'w+') as f:
            f.write(WRONG_NOTATION_TEMPLATE)

        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "RESOURCE_PATH": self.template_path
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_path, "--dry-run=server"],
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_path],
        )
        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with self.caplog.at_level(logging.DEBUG):
                pipe.run()

        self.assertIn("spec file is valid.", self.caplog.text)
        self.assertIn("✔ Pipe finished successfully!", out.getvalue())

    def test_fail_on_validate(self):
        with open(self.template_path, 'w+') as f:
            f.write(INVALID_KUBECTL_TEMPLATE)

        self.mocker.patch.dict(
            os.environ, {
                "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",
                "KUBE_CONFIG": self.kube_config,
                "KUBECTL_COMMAND": "apply",
                "RESOURCE_PATH": self.template_path
            }
        )

        self.fp.register(
            ["kubectl", "apply", "-f", self.template_path, "--dry-run=server"],
            returncode=1
        )

        pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertEqual(pytest_wrapped_e.type, SystemExit)
        self.assertIn("✖ spec file validation failed.", out.getvalue())
