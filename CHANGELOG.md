# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.12.0

- minor: Update kubectl version to 1.31 and awscli version to 2.22.27.
- patch: Internal maintenance: Improve README, add warning about bitbucket branch name length exceeds kubectl label limit.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 3.11.0

- minor: Extend kubectl args with --validate=false when DISABLE_VALIDATION is set to true.

## 3.10.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 3.10.1

- patch: Internal maintenance: Refactor tests.
- patch: Internal maintenance: Update python requirements, pipes versions.

## 3.10.0

- minor: Bump awscli version to 2.15.15.
- minor: Bump kubectl version to 1.29.

## 3.9.1

- patch: Bump bitbucket-pipes-toolkit in setup.py.

## 3.9.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 3.8.1

- patch: Update documentation: fix examples.

## 3.8.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 3.7.0

- minor: Bump kubectl version to 1.26.
- minor: Rebuild image to fix vulnerability with certify.

## 3.6.0

- minor: Internal maintenance: update bitbucket-pipes-toolkit version to 4.0.0.
- patch: Bugfix: fix LABELS not passed correctly.

## 3.5.0

- minor: Add support of the pre-execution script.

## 3.4.1

- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 3.4.0

- minor: Add support for DISABLE_VALIDATION variable. Option to disable Kubernetes manifest file validation before the command execution.
- patch: Internal maintenance: Bump awscli to 2.9.5 in Dockerfile and pipelines configuration file.
- patch: Internal maintenance: Bump docker image to python:3.10-slim-buster in Dockerfile and python:3.10 in pipelines configuration file.
- patch: Internal maintenance: Update community link.
- patch: Internal maintenance: Update release process.
- patch: Internal maintenance: bump packages in test/requirements.txt.

## 3.3.1

- patch: Update pipe's base docker image with a git package.

## 3.3.0

- minor: Bump aws cli version to 2.7.9.
- minor: Bump kubectl cli version to 1.24.

## 3.2.0

- minor: Update documentation in README.
- patch: Internal maintenance: bump version of bitbucket-pipes-toolkit to 3.2.1

## 3.1.2

- patch: Update setup.py in kubectl

## 3.1.1

- patch: Internal maintenance: bump toolkit in python setup.

## 3.1.0

- minor: Bump kubectl cli version to 1.21

## 3.0.1

- patch: Fix update labels for files without metadata.

## 3.0.0

- major: Bump AWS CLI v1 -> v2. For breaking changes refer to https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration.html
- minor: Bump bitbucket-pipes-toolkit -> 2.2.0.

## 2.0.0

- major: Multi-staged build for Dockerfile. Changed base image to python:3.8-slim-buster.

## 1.4.0

- minor: Add kubernetes spec file validation with dry-run=server before apply command.
- minor: Upgrade kubectl cli version to 1.18.

## 1.3.4

- patch: Add KUBECTL_ARGS array validator.

## 1.3.3

- patch: Fix path for kubeconfig file.

## 1.3.2

- patch: Fix bug with LABELS variable. Labels are intended to be used to specify identifying attributes of Kubernetes objects.

## 1.3.1

- patch: Internal maintenance: bump setup.py

## 1.3.0

- minor: Add KUBECTL_APPLY_ARGS support. Allow to use -k kustomization flag.

## 1.2.3

- patch: Internal maintenance: add bitbucket-pipe-release.

## 1.2.2

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 1.2.1

- patch: Extended example KUBE_CONFIG file encoding.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.8

- patch: Add simple validation YAML template.

## 1.1.7

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.6

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.5

- patch: Internal maintenance: Update test infrastructure.

## 1.1.4

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.1.3

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 1.1.2

- patch: Internal maintenance: Add check for newer version.

## 1.1.1

- patch: Fixed the bug with default labels

## 1.1.0

- minor: It is now allowed to pass a directory as a RESOURCE_PATH

## 1.0.0

- major: Changed the SPEC_FILE variable name to RESOURCE_PATH

## 0.1.4

- patch: Documentation improvements.

## 0.1.3

- patch: Fixed the docker image in the pipe.yml

## 0.1.2

- patch: Internal maintenance: increase test coverage

## 0.1.1

- patch: Fix the default label name
- patch: Fixed the default label names
- patch: Internal maintenance: Add dockerfile linter and unittest steps to the pipeline

## 0.1.0

- minor: Initial release
